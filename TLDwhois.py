import urllib #Import urlLib to interact with webpage
import re #Import re for regex matching

# Set 'response' and 'html' to the actual HTML of the webpage
response = urllib.urlopen("https://www.iana.org/domains/root/db")
html = response.read()

# Set 'regex' to find the TLDs for future use and pull all of them
# into the 'matches' variable
regex = '<span class="domain tld">+<a href="\/\w*\/\w*\/\w*\/\w*\.\w*">'
matches = re.findall(regex, html)

# Create empty list
tldURIs = []

# For each entry in the list of matches, trim off the
# unncessary bits and slap 'em into the tldURIs list
for match in matches:
	tldURIs.append(match[34:-2])

# Create empty dictionary for TLDs and their WHOIS servers
tldDictionary = {}

# Initialize the progress counter
progress=0

# For every URI found on the main page, loop
for uri in tldURIs:
	# Set each 'key' in dict to the '.*' value
	key = '.' + uri[17:-5]

	# Generate a full URI for each TLD and retrieve
	newURI = 'https://www.iana.org' + uri
	response = urllib.urlopen(newURI)
	html = response.read()

	# Find place in page for the WHOIS server and match
	regex = 'WHOIS Server:.*'
	matches = re.findall(regex, html)

	# If a match is found continue, or else the TLD
	# Does not have a WHOIS server and should be
	# ignored.
	if matches:
		# Entry will be the first, trim the first 18
		# Characters to remove the B.S.
		match = matches[0]
		match = match[18:]

		# Add entry to the dictionary of the '.*' for the key
		# and the WHOIS server for the match
		tldDictionary[key] = match

		# The following is for progress notification
		print 'WHOIS server found for ' + key + ' at ' + match
		progress += 1
		if (progress % 50) == 0:
			print 'Number of domains found: ' + str(progress)

# Once completed print out the dictionary
print tldDictionary

'''
if os.path.isfile('tld.csv'):

else:
	headings = ['tld','WHOIS Server']
	path = os.getcwd() + '\\tld.csv'
'''